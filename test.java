
package uno;

import java.util.List;

public class nweiss2UnoPlayer implements UnoPlayer {

    /**
     * Compare la carte en jeu avec celles de la main du joueur et sélectionne
     * la carte la plus adaptée en fonction du rang et de la couleur de cette
     * carte
     * @param hand
     * @param upCard
     * @param calledColor
     * @param state
     * @return i ( Numéro dans la main de la carte à jouer )
     */
    public int play(List<Card> hand, Card upCard, Color calledColor,
        GameState state) {
          switch(upCard.getColor()){
            case RED:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.RED
                && (hand.get(i)).getRank()==Rank.DRAW_TWO){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.RED
                && (hand.get(i)).getRank()==Rank.SKIP){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.RED
                && (hand.get(i)).getRank()==Rank.REVERSE){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.RED){
                  return i;
                }
              }
              break;
            case BLUE:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.BLUE
                && (hand.get(i)).getRank()==Rank.DRAW_TWO){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.BLUE
                && (hand.get(i)).getRank()==Rank.SKIP){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.BLUE
                && (hand.get(i)).getRank()==Rank.REVERSE){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.BLUE){
                  return i;
                }
              }
              break;
            case YELLOW:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.YELLOW
                && (hand.get(i)).getRank()==Rank.DRAW_TWO){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.YELLOW
                && (hand.get(i)).getRank()==Rank.SKIP){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.YELLOW
                && (hand.get(i)).getRank()==Rank.REVERSE){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.YELLOW){
                  return i;
                }
              }
              break;
            case GREEN:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.GREEN
                && (hand.get(i)).getRank()==Rank.DRAW_TWO){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.GREEN
                && (hand.get(i)).getRank()==Rank.SKIP){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.GREEN
                && (hand.get(i)).getRank()==Rank.REVERSE){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==Color.GREEN){
                  return i;
                }
              }
              break;
          }
          switch(upCard.getRank()){
            case DRAW_TWO:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getRank()==Rank.DRAW_TWO){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if( (hand.get(i)).getRank()==Rank.WILD_D4){
                  return i;
                }
              }
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==upCard.getColor()
                || (hand.get(i)).getColor()==Color.NONE){
                  return i;
                }
              }
              break;
            case SKIP:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getRank()==Rank.SKIP
                || (hand.get(i)).getColor()==upCard.getColor()
                || (hand.get(i)).getColor()==Color.NONE){
                  return i;
                }
              }
              break;
            case NUMBER:
              switch(upCard.getNumber()){
                case 0:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==0){
                      return i;
                    }
                  }
                  break;
                case 1:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==1){
                      return i;
                    }
                  }
                  break;
                case 2:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==2){
                      return i;
                    }
                  }
                  break;
                case 3:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==3){
                      return i;
                    }
                  }
                  break;
                case 4:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==4){
                      return i;
                    }
                  }
                  break;
                case 5:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==5){
                      return i;
                    }
                  }
                  break;
                case 6:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==6){
                      return i;
                    }
                  }
                  break;
                case 7:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==7){
                      return i;
                    }
                  }
                  break;
                case 8:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==8){
                      return i;
                    }
                  }
                  break;
                case 9:
                  for (int i=0;i<=hand.size()-1;i++){
                    if ((hand.get(i)).getNumber()==9){
                      return i;
                    }
                  }
                  break;
              }
            case WILD:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==calledColor
                || (hand.get(i)).getColor()==Color.NONE){
                  return i;
                }
              }
              break;
            case WILD_D4:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getColor()==calledColor
                || (hand.get(i)).getColor()==Color.NONE){
                  return i;
                }
              }
              break;
            case REVERSE:
              for (int i=0;i<=hand.size()-1;i++){
                if ((hand.get(i)).getRank()==Rank.REVERSE
                || (hand.get(i)).getColor()==upCard.getColor()
                || (hand.get(i)).getColor()==Color.NONE){
                  return i;
                }
              }
              break;
          }
        return -1;
    }

    /**
     * Cherche la couleur la plus adaptée et l'impose au prochain joueur lors
     * du tirage d'une carte joker
     * @param hand
     * @return Color ( Couleur imposée pour le prochain joueur )
     */
    public Color callColor(List<Card> hand) {
      int[] tabCouleursInHand = {0, 0, 0, 0};
      int max = 0;
      for (int i=0;i<=hand.size()-1;i++){
        if ((hand.get(i)).getColor()==Color.RED){
          tabCouleursInHand[0] += 1;
        }
        if ((hand.get(i)).getColor()==Color.BLUE){
          tabCouleursInHand[1] += 1;
        }
        if ((hand.get(i)).getColor()==Color.YELLOW){
          tabCouleursInHand[2] += 1;
        }
        if ((hand.get(i)).getColor()==Color.GREEN){
          tabCouleursInHand[3] += 1;
        }
      }
      for (int i=0; i<=tabCouleursInHand.length-1; i++){
        if (tabCouleursInHand[i]>max){
          max = i;
        }
      }
      switch (max){
        case 0: return Color.RED;
        case 1: return Color.BLUE;
        case 2: return Color.YELLOW;
        case 3: return Color.GREEN;
      }
      return null;
    }
  }
